import React, { Component } from 'react'
import { Loader } from 'semantic-ui-react'

export const withCurrentProject = (WrappedComponent) => {
    return class withCurrentProject extends Component {
        constructor(props) {
            super(props)
            this.state= {
                currentProject: {},
                projectid: this.props.match.params.projectid
            }
        }

        render() {
            // Richiede il progetto corrente se non lo si ha già
            if (Object.keys(this.state.currentProject).length === 0) {
                let query = `
                query fetchCurrentProject {
                    projects(id: ${this.state.projectid}) {
                        data {
                            id
                            proj_name
                            proj_owner
                            proj_share
                            proj_profile
                            proj_layout
                            proj_blocked
                            proj_note
                        }
                    }
                }
                `
                this.props.apolloFetch({ query })
                    .then((data) => {
                        this.setState({currentProject: data.data.projects.data[0]})
                    })
                    .catch((error) => {
                        console.log(error);
                    })

                return (
                    <Loader active size='massive' inline='centered' />
                )
            }
            else {
                return (
                    <div>
                        <WrappedComponent {...this.props} project={this.state.currentProject} />
                    </div>
                )
            }
        }
    }
}
