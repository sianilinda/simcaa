import React, { Component, Fragment } from 'react'
import { Card, Input, Image, Label, Transition, Segment, Loader, Divider, Icon } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import { connect } from 'react-redux'
import { AddNewBlankCard, DeleteCard, CardUINavbarCard, toggleVisibilityImgAlt, mergeCard, setImg, setNewImg } from '../../store/actions/CardUIActions'
import { chapterContentDataFetchDataSuccess } from '../../store/actions/ChapterFetchActions'
import { addNewRow } from '../../store/actions/CardUIActions'
import { setFocus, setExpandAll, setLineSpacing } from '../../store/functionUtils'

class CardUI extends Component {
    constructor(props) {
        super(props)
        this.state = {
            card: props.cards,
            borderCard: {},
        }
    }

    componentDidMount() {
        this.checkPropsColorCard()
        let styleCard = this.props.borderCard ?
        {
            Aggettivo: {border: this.props.borderCard['Aggettivo'].size + 'px ' + this.props.borderCard['Aggettivo'].type + ' ' + this.props.borderCard['Aggettivo'].color},
            Articolo: {border: this.props.borderCard['Articolo'].size + 'px ' + this.props.borderCard['Articolo'].type + ' ' + this.props.borderCard['Articolo'].color},
            Avverbio: {border: this.props.borderCard['Avverbio'].size + 'px ' + this.props.borderCard['Avverbio'].type + ' ' + this.props.borderCard['Avverbio'].color},
            Congiunzione: {border: this.props.borderCard['Congiunzione'].size + 'px ' + this.props.borderCard['Congiunzione'].type + ' ' + this.props.borderCard['Congiunzione'].color},
            Interiezione: {border: this.props.borderCard['Interiezione'].size + 'px ' + this.props.borderCard['Interiezione'].type + ' ' + this.props.borderCard['Interiezione'].color},
            Pronome: {border: this.props.borderCard['Pronome'].size + 'px ' + this.props.borderCard['Pronome'].type + ' ' + this.props.borderCard['Pronome'].color},
            Preposizione: {border: this.props.borderCard['Preposizione'].size + 'px ' + this.props.borderCard['Preposizione'].type + ' ' + this.props.borderCard['Preposizione'].color},
            Sostantivo: {border: this.props.borderCard['Sostantivo'].size + 'px ' + this.props.borderCard['Sostantivo'].type + ' ' + this.props.borderCard['Sostantivo'].color},
            Verbo: {border: this.props.borderCard['Verbo'].size + 'px ' + this.props.borderCard['Verbo'].type + ' ' + this.props.borderCard['Verbo'].color},
            Altro: {border: this.props.borderCard['Altro'].size + 'px ' + this.props.borderCard['Altro'].type + ' ' + this.props.borderCard['Altro'].color}
        } : {}
        this.setState({borderCard: styleCard})
        setLineSpacing(this.props.margins)
    }

    componentWillMount() {
        // Controllo se c'è una card fissa
        if (this.checkPropsCard() === true) {
            this.setState({card: this.props.Card})
        }
    }

    componentDidUpdate() {
        this.checkPropsColorCard()
    }

    // Setto il colore delle input in caso di profilo non base
    checkPropsColorCard() {
        let nCard = document.getElementsByClassName('cardUI')
        for (let i = 0; i < nCard.length; i++) {
            let cardElement = document.getElementById('text-' + i)
            if (cardElement) {
                cardElement.style.setProperty('color', this.props.colorTextInput, 'important')
                cardElement.style.setProperty('background-color', this.props.colorBackgroundInput, 'important')
            }
        }
    }

    // Guardo se c'è un card fissa oppure no
    checkPropsCard() {
        if (this.props.Card) {
            return true
        }
        else {
            return false
        }
    }

    // Handle click sulla Card
    handleClickCard(index, e) {
        this.props.setNavbarCard(index)
        setFocus(index)
    }

    // Allinea correttamente il focus della card in caso di caratteri speciali
    handleSpecialKey(currentCard, e) {
        if (e.keyCode === 9 || e.keyCode === 16) {
            this.props.setNavbarCard(currentCard.id)
        }
    }

    // Cambio il lemma in base alla input
    handleChange(currentCard, input) {
        let localCards = this.props.cards.slice()
        localCards[currentCard.id].lemmaPrevious = localCards[currentCard.id].lemma
        localCards[currentCard.id].lemma = input.target.value
        this.props.updateCardArray(localCards)
        this.props.setNavbarCard(currentCard.id)
    }

    // Main loop onKeyDown
    handleSetCard(card, input) {
        let index = this.props.navbarCard.id
        let localCards = this.props.cards.slice()
        let currentCard = localCards[index]
        let found = false
        if ((input.keyCode === 32 && currentCard.lock === 'unlock') || input.keyCode === 13) {
            input.preventDefault()
            if (currentCard.lemma) {
                // Controlla se è un watermark o carattere speciale
                for (let i = 0; i < this.props.watermark.length; i++) {
                    if (currentCard.lemma === this.props.watermark[i].namewater) {
                        currentCard.img = this.props.watermark[i].filewater
                        currentCard.custom = false
                        currentCard.water = true
                        found = true
                    }
                }

                // Controlla se esiste già una card con lo stesso lemma al fine di evitare di fare la chiamata al db
                for (let i = 0; i < localCards.length; i++) {
                    if (localCards[i].lemma === currentCard.lemma && localCards[i].id !== currentCard.id) {
                        currentCard.img = localCards[i].img
                        currentCard.sinonimi = localCards[i].sinonimi
                        currentCard.imgAlt = localCards[i].imgAlt
                        currentCard.lock = localCards[i].lock
                        currentCard.codClass = localCards[i].codClass
                        currentCard.complex = localCards[i].complex
                        currentCard.custom = localCards[i].custom
                        found = true
                    }
                }

                // Se la card è bloccata non fa la ricerca del simbolo
                if (currentCard.lock === 'lock') {
                    found = true
                }

                // Se ci sono spazi nel lemma non fa la ricerca del simbolo
                if (currentCard.lemma.indexOf(' ') >= 0) {
                    found = true
                }

                // Se il lemma non è cambiato non fa la ricerca del simbolo
                if (currentCard.lemma === currentCard.lemmaPrevious) {
                    found = true
                }

                // In caso i controlli precedenti falliscano esegue la chiamata al db
                if (input.target.value !== '' && !found && currentCard.complex === '0') {
                    this.props.setImg(localCards, index, input.target.value, currentCard.row, true, this.props.imgType, this.props.imgStyle, this.props.priorityOrder)
                }

                // Shifto tutte le card dopo nella nuova riga in caso di invio in mezzo alla frase
                if (input.keyCode === 13 && localCards[index + 1]) {
                    for (let i = index + 1; i <= localCards[localCards.length - 1].id; i++) {
                        localCards[i].row = localCards[i].row + 1
                    }
                    this.props.updateCardArray(localCards)
                }

                // Crea la nuova card vuota
                if (!localCards[index + 1] || localCards[index + 1].lemma) {
                    let newRow = currentCard.row
                    if (input.keyCode === 13) {
                        newRow++
                        this.props.updateRowsArray(currentCard.row + 1, 'new_line')
                    }
                    this.props.AddNewBlankCard(index + 1, newRow)
                }
                setFocus(index + 1)
            }
        }
        if (input.keyCode === 8 && localCards.length > 1) {
            let start = input.target.selectionStart
            if (start === 0) {
                input.preventDefault()
                this.props.DeleteCard(index)
            }
        }
        found = false
        setExpandAll(localCards)
        setLineSpacing(this.props.margins)
    }

    // Get watermark image name
    getWatermarkSrc(watermark) {
        let index = this.props.watermark.findIndex(x => x.namewater === 'wm:' + watermark)
        return window.env.WaterImages + this.props.watermark[index].filewater
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        // render the Cards Array to display
        let cards = this.props.cards.slice()
        let inputBorder = this.props.transparent === 'normal' ? false : true
        cards = cards.map((item, index) => {
            let testCustom = item.imgAlt.find(x => x.img === item.img)
            let srcImg = testCustom && testCustom.custom ? window.env.CustomImage + item.img : window.env.PathImages + item.img

            let styles = {}
            if (this.props.cardStyle && this.props.borderCard) {
                styles = Object.assign({}, this.props.cardStyle, this.state.borderCard[item.codClass])
            }
            else if (this.props.cardStyle) {
                styles = this.props.cardStyle
            }
            else if (this.props.borderCard) {
                styles = this.state.borderCard[item.codClass]
            }

            let imageLoading
            if (this.props.cardIsLoading && this.props.navbarCard.id === index) {
                imageLoading = <Segment basic className='imgpadding0'><Loader active />
                                <Image src={item.water ? window.env.WaterImages + item.img : srcImg} size={this.props.imgSize} className='card-image'/></Segment>
            } else {
                imageLoading =  <Image src={item.water ? window.env.WaterImages + item.img : srcImg} size={this.props.imgSize} className='card-image'/>
            }

            let watermark = <Fragment>
                            {item.watermark && item.watermark.negazione ? <Image src={this.getWatermarkSrc('negazione')} size={this.props.imgSize} className='watermark-negated'/> : null}
                            {item.watermark && item.watermark.plurale ? <Image src={this.getWatermarkSrc('plurale')} size={this.props.imgSize} className='watermark-all'/> : null}
                            {item.watermark && item.watermark.passato ? <Image src={this.getWatermarkSrc('passato')} size={this.props.imgSize} className='watermark-all'/> : null}
                            {item.watermark && item.watermark.futuro ? <Image src={this.getWatermarkSrc('futuro')} size={this.props.imgSize} className='watermark-all'/> : null}
                        </Fragment>

            let classNameInput
            if (item.water) {
                classNameInput = this.props.formatInput + ' ' + this.props.weightInput + ' ' + this.props.decorationInput + ' ' + this.props.fontStyleInput + ' colorTextInput colorBackgroundInput input-water'
            } else {
                classNameInput = this.props.formatInput + ' ' + this.props.weightInput + ' ' + this.props.decorationInput + ' ' + this.props.fontStyleInput + ' colorTextInput colorBackgroundInput'
            }
            let cardInput = (
              <Card.Content>
                <Input
                  size={this.props.sizeInput}
                  disabled={this.props.mode}
                  transparent={inputBorder}
                  className={classNameInput}
                  id={"text-" + item.id}
                  value={item.lemma}
                  onChange={this.handleChange.bind(this, item)}
                  onKeyDown={this.handleSetCard.bind(this, item)}
                  onKeyUp={this.handleSpecialKey.bind(this, item)}
                />
              </Card.Content>
            );

            let classNameCard = item.water ? 'cardUI cardUI-water' : 'cardUI'
            if (this.props.posInput === 'bottom') {
                return(
                    <div key={'' + item.id + ''} className={this.props.disabledCard}
                        onClick={this.handleClickCard.bind(this, index)}>
                        <Card
                            id={'card-' + item.id}
                            className={classNameCard}
                            style={{...styles}}
                            color={item.id === this.props.navbarCard.id && !item.water ? 'blue' : null}
                        >
                            <Card.Content className={`position-relative ${this.props.imgPadding}`}>
                                {imageLoading}
                                {watermark}
                            </Card.Content>
                            {cardInput}
                        </Card>
                    </div>
                )
            }
            else if (this.props.posInput === 'top') {
                return(
                    <div key={item.id} className={this.props.disabledCard}
                        onClick={this.handleClickCard.bind(this, index)}>
                        <Card
                            id={'card-' + item.id}
                            className={classNameCard}
                            style={{...styles}}
                            color={item.id === this.props.navbarCard.id && !item.water ? 'blue' : null}
                        >
                            {cardInput}
                            <Card.Content className={`position-relative ${this.props.imgPadding}`}>
                                {imageLoading}
                                {watermark}
                            </Card.Content>
                        </Card>
                    </div>
                )
            }
        })

        // render the Row
        let cardGroup = []
        if (this.props.cards.length !== 0) {
            for (let i = 0; i <= this.props.cards[this.props.cards.length-1].row; i++) {
                cardGroup.push(this.props.cards[this.props.cards.length-1].row)
            }

            cardGroup = cardGroup.map((item, index) => {
                let cardPerRow = []
                for (let i = 0; i < this.props.cards.length; i++) {
                    if (this.props.cards[i].row === index) {
                        cardPerRow.push(cards[i])
                    }
                }

                let marginNewLine = this.props.margins && this.props.rows.length > 0 && this.props.rows[index]? this.props.margins[this.props.rows[index].type] : 0
                let multiplier = this.props.rows.length > 0 && this.props.rows[index] ? this.props.rows[index].times : 1
                marginNewLine = marginNewLine * multiplier
                marginNewLine = index === 0 ? 0 : marginNewLine

                marginNewLine = this.props.rowStyle && this.props.rowStyle.marginTop && index === 0 ? marginNewLine + parseInt(this.props.rowStyle.marginTop) : marginNewLine
                let style = {marginTop: marginNewLine + 'px'}

                let divider = null
                if (this.props.rows.length > 0 && this.props.rows[index + 1] && this.props.rows[index + 1].page) {
                    divider = <Divider horizontal>{this.props.t("LBL_PAGE")}</Divider>
                }

                return(
                    <Fragment key={index}>
                        <Card.Group className='cardLine' style={style}>
                            <Icon name='minus' color='grey' />
                            {cardPerRow}
                        </Card.Group>
                        {divider}
                    </Fragment>
                )
            })
        }

        // render the div for the alternative imgs
        let imagesDiv
        if (Object.keys(this.props.navbarCard).length !== 0 && this.props.navbarCard.constructor === Object) {
            imagesDiv = this.props.navbarCard.imgAlt.slice()
        } else {
            imagesDiv = []
        }
        imagesDiv = imagesDiv.map((item, index) => {
            let srcImg = item.custom ? window.env.CustomImage + item.img : window.env.PathImages + item.img
            return(
                <Image
                    src={srcImg}
                    style={{margin: "10px"}}
                    width={'100'}
                    height={'100'}
                    alt="test"
                    key={index}
                    label={item.voice_human}
                    className='alt-img-edit'
                    onClick={(e) => {this.props.setNewImg(e, item)}}
                />
            )
        })

        return(
            <div>
                {cardGroup}
                <Transition visible={this.props.visibilityImgAlt} animation='slide up' duration={500}>
                    <Segment
                        inverted
                        color="green"
                        className="footer"
                        style={{zIndex: '100'}}
                    >
                        <Label attached='top right' color='red'
                            onClick={() => {this.props.toggleVisibilityImgAlt(false)}}
                            className='icon-pointer'
                        >
                            Close
                        </Label>
                        <Image.Group>
                            {imagesDiv}
                        </Image.Group>
                    </Segment>
                </Transition>
            </div>
        )}
}


const mapStateToProps = (state) => {
    return {
        cards: state.chapterContentData,
        rows: state.chapterRowsData,
        navbarCard: state.CardUINavbarCard,
        project: state.projectData,
        visibilityImgAlt: state.toggleVisibilityImgAlt,
        cardIsLoading: state.CardUIsearchIsLoading,
        watermark: state.watermarkData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        AddNewBlankCard: (index, row) => dispatch(AddNewBlankCard(index, row)),
        DeleteCard: (index) => dispatch(DeleteCard(index)),
        setNavbarCard: (value) => dispatch(CardUINavbarCard(value)),
        updateCardArray: (array) => dispatch(chapterContentDataFetchDataSuccess(array)),
        updateRowsArray: (row, type) => dispatch(addNewRow(row, type)),
        toggleVisibilityImgAlt: (value) => dispatch(toggleVisibilityImgAlt(value)),
        setNewImg: (e, newImg) => dispatch(setNewImg(e, newImg)),
        mergeCard: (direction, id , lemma) => dispatch(mergeCard(direction, id , lemma)),
        setImg: (data, index, lemma, row, NoAuth, imgType, imgStyle, priorityOrder) => dispatch(setImg(data, index, lemma, row, NoAuth, imgType, imgStyle, priorityOrder)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(CardUI))
