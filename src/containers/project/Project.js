import React, { Component } from 'react'
import { Button, Menu, Card, Dimmer, Loader, Breadcrumb } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'

import NewProfileForm from '../homepage/NewProfileForm'
import NewChapterForm from '../../components/project/NewChapterForm'
import ChapterUI from '../../components/project/ChapterUI'
import Can from '../Permission'

import { escapeQuotes } from '../../store/functionUtils'
import { connect } from 'react-redux'
import { chapterDataFetchData, chapterDataCreateData, chapterDataDeleteData, chapterDataUpdateData, chapterDataFetchDataSuccess, duplicateChapter } from '../../store/actions/ChapterFetchActions'
import { projectDataUpdateData } from '../../store/actions/ProjectFetchActions'
import { selectedProjectID } from '../../store/actions/CardUIActions'

class Project extends Component {
    componentDidMount() {
        let currentProjectId = this.props.match.params.projectid
        this.props.fetchChapter(null, 30, 1, currentProjectId)
        this.props.selectedProjectID(currentProjectId)
    }

    // Create a new chapter
    createNewChapter(title) {
        let currentProjectId = this.props.match.params.projectid
        let escapedTitle = escapeQuotes(title)
        let query = `
        mutation NewChapter {
            createCaaChapter(caa_project_id: ${currentProjectId},
                            chapt_title: "${escapedTitle}",
                            chapt_content: "",
                            chapt_row: "",
                            chapt_user_block: 0) {
                id
            }
        }
        `
        this.props.createChapter(query, currentProjectId)
    }

    // Delete the selected chapter
    deleteChapter(id, event) {
        let index = this.props.project.findIndex(x => x.proj_id === parseInt(this.props.match.params.projectid, 10))
        if (this.props.project[index].proj_owner === this.props.user.id) {
            this.props.deleteChapter(id, this.props.match.params.projectid)
        }
    }

    // Update the profile for the current project
    updateProjectProfile(data) {
        let index = this.props.project.findIndex(x => x.proj_id === parseInt(this.props.match.params.projectid, 10))
        let proj_profile = escapeQuotes(data)
        let proj_name = escapeQuotes(this.props.project[index].proj_name)
        let proj_note = escapeQuotes(this.props.project[index].proj_note)
        let query = `
        mutation updateProject {
            updateCaaProject(id: ${this.props.project[index].proj_id},
                            proj_name: "${proj_name}",
                            proj_owner: ${this.props.project[index].proj_owner},
                            proj_share: ${this.props.project[index].proj_share},
                            proj_profile: "${proj_profile}",
                            proj_note: "${proj_note}"){
                id
            }
        }
        `
        this.props.updateProfileProject(query)
    }

    // Force unlock of the chapter if the current user is the admin or the project owner
    forceUnlockChapter(id, event) {
        let query = `
        mutation UnlockChapter {
            updateCaaChapter(id: ${id}, chapt_user_block: 0) {
                id
            }
        }
        `
        this.props.updateChapter(query, this.props.match.params.projectid)
    }

    // redirect to homepage and clean the chapter list/current selected project
    redirectProject() {
        this.props.resetProject([])
        this.props.selectedProjectID(-1)
        this.props.history.push('/home')
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        var prjID = this.props.project.findIndex(x => x.proj_id === parseInt(this.props.match.params.projectid, 10))
        if (prjID < 0) {
            prjID = 0
        }
        if (this.props.chaptHasErrored || this.props.chaptIsLoading) {
            return (
                <Dimmer
                    active={this.props.chaptIsLoading}
                    page
                >
                    <Loader active inline='centered' size='massive' />
                </Dimmer>
            )
        }

        let localChapters = this.props.chapters
        localChapters = localChapters.map((item, index) => {
            return (
                <ChapterUI
                    key={index}
                    forceUnlockChapter={this.forceUnlockChapter.bind(this)}
                    deleteChapter={this.deleteChapter.bind(this)}
                    duplicateChapter={this.props.duplicateChapter.bind(this)}
                    updateChapter={this.props.updateChapter.bind(this)}
                    chapter={item}
                    user={this.props.user}
                    projectId={parseInt(this.props.match.params.projectid, 10)}
                    projectOwner={this.props.project[prjID].proj_owner}
                    projectBlocked={this.props.project[prjID].proj_blocked}
                />
            )
        })

        let isProjBlockedOrNotOwner = this.props.project[prjID].proj_blocked === 1 ? true : false
        if (this.props.project[prjID].proj_owner === this.props.user.id) {
            isProjBlockedOrNotOwner = false
        }

        return (
            <div>
                <Menu>
                    <Menu.Item>
                        <Button color='red' onClick={this.redirectProject.bind(this)}>{t("PRJ_MNU_BACK")}</Button>
                    </Menu.Item>
                    <Menu.Item disabled={isProjBlockedOrNotOwner}>
                        <NewProfileForm
                            className='icon-pointer'
                            size='big'
                            edit='button'
                            data={this.props.project[prjID].proj_profile}
                            updateProjectProfile={this.updateProjectProfile.bind(this)}
                        />
                    </Menu.Item>
                    <Menu.Item>
                        <Breadcrumb>
                            <Breadcrumb.Section link onClick={this.redirectProject.bind(this)}>home</Breadcrumb.Section>
                            <Breadcrumb.Divider />
                            <Breadcrumb.Section active>{this.props.project[prjID].proj_name}</Breadcrumb.Section>
                        </Breadcrumb>
                    </Menu.Item>
                    <Menu.Item>
                        {'(Progetto: ' + this.props.match.params.projectid + ')'}
                    </Menu.Item>
                </Menu>

                <Card.Group style={{'marginLeft': '10px'}}>
                    {localChapters}
                    {
                        !isProjBlockedOrNotOwner ?
                        <Can perform='can_create_project' or='can_manage_project'>
                            <Card style={{'textAlign': 'center'}}>
                                <Card.Content header={t("PRJ_LBL_NEWCHAPT")}/>
                                <Card.Content>
                                    <NewChapterForm
                                        createChapter={this.createNewChapter.bind(this)}
                                        className='icon-pointer'
                                        nameIcon='add square'
                                        sizeIcon='huge'
                                    />
                                </Card.Content>
                                <Card.Content extra>
                                    {t("PRJ_LBL_INFO")}
                                </Card.Content>
                            </Card>
                        </Can>
                        : null

                    }
                </Card.Group>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        chapters: state.chapterData,
        project: state.projectData,
        chaptIsLoading: state.chapterDataIsLoading,
        chaptHasErrored: state.chapterDataHasErrored,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchChapter: (query, limit, page, id) => dispatch(chapterDataFetchData(query, limit, page, id)),
        createChapter: (mutation, projectId) => dispatch(chapterDataCreateData(mutation, projectId)),
        updateChapter: (mutation, projectId) => dispatch(chapterDataUpdateData(mutation, projectId)),
        deleteChapter: (id, projectId) => dispatch(chapterDataDeleteData(id, projectId)),
        updateProfileProject: (mutation) => dispatch(projectDataUpdateData(mutation)),
        resetProject: (value) => dispatch(chapterDataFetchDataSuccess(value)),
        duplicateChapter: (id) => dispatch(duplicateChapter(id)),
        selectedProjectID: (id) => dispatch(selectedProjectID(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(translate('translations')(Project)))
