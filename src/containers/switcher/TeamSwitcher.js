import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import { connect } from 'react-redux'
import { dispatchFilter } from '../../store/actions/FilterActions'
import { projectDataFetchData } from '../../store/actions/ProjectFetchActions'
import { profileDataFetchData } from '../../store/actions/ProfileFetchActions'
import { membersTeamDataFetchData } from '../../store/actions/UserTeamFetchActions'

class TeamSwitcher extends Component {
    onChangeTeamValue(e, data) {
        this.props.changeFilter(null, data.value)
        this.props.fetchProfile()
        this.props.memberTeam()
    }

    menuChangeTeam(data, e) {
        this.props.changeFilter(null, data)
        this.props.fetchProject(null, 15, 1)
        this.props.fetchProfile()
        this.props.memberTeam()
    }

    render() {
        const { t } = this.props

        let dropdownTeamOptions = []
        this.props.team.forEach((item) => {
            dropdownTeamOptions.push({'text': item.team_name, 'value': item.team_id})
        })

        let indexTeam = this.props.team.findIndex(x => x.team_id === this.props.teamID)
        if (this.props.type === 'menu') {
            let menuOptions = dropdownTeamOptions
            menuOptions = menuOptions.map((item, index) => {
                return (
                    <Dropdown.Item
                        onClick={this.menuChangeTeam.bind(this, item.value)}
                        key={index}
                    >
                        {item.text}
                    </Dropdown.Item>
                )
            })
            return (
                // <Dropdown item text={this.props.team[indexTeam] ? this.props.team[indexTeam].team_name : ''}>
                <Dropdown item text={this.props.team[indexTeam].team_name}>
                    <Dropdown.Menu>
                        {menuOptions}
                    </Dropdown.Menu>
                </Dropdown>
            )
        }

        return (
            <Dropdown
                style={this.props.style}
                selection
                placeholder={t("TMSEL_DROP_PLACEHOLDER")}
                options={dropdownTeamOptions}
                defaultValue={this.props.defaultValue}
                onChange={this.onChangeTeamValue.bind(this)}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        team: state.userTeamData,
        teamID: state.filterValue,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeFilter: (mode, value) => dispatch(dispatchFilter(mode, value)),
        fetchProject: (query, limit, page) => dispatch(projectDataFetchData(query, limit, page)),
        fetchProfile: () => dispatch(profileDataFetchData()),
        memberTeam: (query) => dispatch(membersTeamDataFetchData(query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(TeamSwitcher))
