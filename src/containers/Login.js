import React, { Component } from 'react'
import { Container, Grid, Segment, Card, Icon, Image, Modal, Message, Dimmer } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import LoginForm from '../components/LoginForm'
import { translate } from 'react-i18next'

import { connect } from 'react-redux'
import { jwtFetchData } from '../store/actions/JWTActions'
import { dispatchFilter } from '../store/actions/FilterActions'
import ContactForm from '../components/ContactForm'

import logo from '../assets/simcaa.png'
import arasaac from '../assets/credits/arasaac.png'
import responsive from '../assets/credits/responsive.png'
import social from '../assets/sponsor/fondazione_social.png'
import ils from '../assets/sponsor/ils.png'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
             dimmerActive: false,
        }
    }
    

    componentDidUpdate(prevProps) {
        if (this.props.userIsLoading === false && this.props.userHasErrored === false && this.props.userToHome === true) {
            if (this.props.user.status === 0) {
                this.props.history.push('/init')
            } else if(!this.state.dimmerActive) {
                this.handleOpenCloseDimmer()
            }
        }
    }

    componentDidMount() {
        let message = document.getElementById('homepage_message')
        message.innerHTML = window.homepage_document.homepage_message
    }

    // Open/close dimmer on page
    handleOpenCloseDimmer() {
        this.setState({dimmerActive: !this.state.dimmerActive})
    }

    // Call the login/jwt action
    login(user, password) {
        this.props.changeFilter('private')
        this.props.fetchJWT(window.env.GraphQLLogin, user, password)
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        const message = { text: 'Fill the form to Login into the App', view: this.props.jwtHasErrored, header: 'Welcome to SimCAA'}
        return (
            <Container fluid className='full-height'>
                <Dimmer page active={this.state.dimmerActive} onClickOutside={this.handleOpenCloseDimmer.bind(this)}>
                    <Segment color='red' inverted>IL TUO UTENTE È BLOCCATO, CONTATTA UN ADMIN PER RISOLVERE</Segment>
                </Dimmer>
                <Grid columns='equal' stackable>
                    <Grid.Column>
                        <Segment basic>
                            <Image src={logo} centered />
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            <LoginForm login={this.login.bind(this)} message={message} loading={this.props.jwtIsLoading}/>
                        </Segment>
                    </Grid.Column>
                </Grid>
                <Message positive style={{width: '80%', margin: 'auto'}} id='homepage_message' />
                <Grid columns='equal' padded stackable>
                    <Grid.Column>
                        <Segment basic>
                            <Card centered color='red'>
                                <Card.Content textAlign='center'>
                                    <Card.Header>{window.homepage_document.box1_title}</Card.Header>
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                    <Icon name={window.homepage_document.box1_icon} size='huge' />
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                    {t("LGN_TXT_PRIVATE")}
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                    <ContactForm color='blue' type='request_account' account='privato' />
                                </Card.Content>
                            </Card>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            <Card centered color='yellow'>
                                <Card.Content textAlign='center'>
                                    <Card.Header>{window.homepage_document.box2_title}</Card.Header>
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                    <Icon name={window.homepage_document.box2_icon} size='huge' />
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                {t("LGN_TXT_SCHOOL")}
                                </Card.Content>
                                <Card.Content extra textAlign='center'>
                                    <ContactForm color='blue' type='request_account' account='scuole' />
                                </Card.Content>
                            </Card>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            <Card centered color='teal'>
                                <Card.Content textAlign='center'>
                                    <Card.Header>{window.homepage_document.box3_title}</Card.Header>
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                    <Icon name={window.homepage_document.box3_icon} size='huge' />
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                {t("LGN_TXT_PRO")}
                                </Card.Content>
                                <Card.Content extra textAlign='center'>
                                    <ContactForm color='blue' type='request_account' account='professionisti' />
                                </Card.Content>
                            </Card>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment basic>
                            <Card centered color='purple'>
                                <Card.Content textAlign='center'>
                                    <Card.Header>{window.homepage_document.box4_title}</Card.Header>
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                    <Icon name={window.homepage_document.box4_icon} size='huge' />
                                </Card.Content>
                                <Card.Content textAlign='center'>
                                {t("LGN_TXT_TUTORIAL")}
                                </Card.Content>
                                <Card.Content extra textAlign='center'>
                                    {/* <Button color='blue'>
                                        Approve
                                    </Button> */}
                                </Card.Content>
                            </Card>
                        </Segment>
                    </Grid.Column>
                </Grid>
                <Grid columns='equal' stackable>
                    <Grid.Column>
                        <Segment textAlign='center' basic>
                            SPONSOR
                            <Image.Group size='medium'>
                                <Image src={social} />
                                <Image src={ils} />
                            </Image.Group>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <div style={{textAlign: 'center'}}>CREDITS</div>
                        <Grid columns='equal'>
                            <Grid.Column>
                                <Segment textAlign='center' basic>
                                    <Image.Group size='medium'>
                                        <Image src={arasaac} />
                                        {/* <Image src={responsive} /> */}
                                    </Image.Group>
                                </Segment>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment basic>
                                    <div style={{width: '300px', verticalAlign: 'top', fontFamily: 'Arial', fontSize: '9pt', lineHeight: 'normal'}}>
                                        <a rel="license" href="//responsivevoice.org/">
                                            <img title="ResponsiveVoice Text To Speech" src="https://responsivevoice.org/wp-content/uploads/2014/08/120x31.png" style={{float: 'left', paddingRight: '2px'}} />
                                        </a>
                                        <span>
                                            <a href="//responsivevoice.org/" target="_blank" title="ResponsiveVoice Text To Speech">
                                                ResponsiveVoice
                                            </a>
                                        </span>
                                        &ensp;used under
                                        <a rel="license" href="https://creativecommons.org/licenses/by-nc-nd/4.0/" title="Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License">
                                            &ensp;Non-Commercial License
                                        </a>
                                    </div>
                                    <div style={{clear: 'both'}}>&nbsp;</div>
                                </Segment>
                            </Grid.Column>
                        </Grid>  
                    </Grid.Column>
                </Grid>
                <Grid columns='equal' stackable>
                    <Grid.Column>
                        <Segment textAlign='center' basic>
                            &copy; Simcaa Tutti i diritti sono riservati
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Modal closeIcon 
                            trigger={
                            <Segment textAlign='center' basic>
                                <p className='similar-link'>Termini e condizioni</p>
                            </Segment>
                        }>
                            <Modal.Header>Termini e condizioni</Modal.Header>
                            <Modal.Content id='conditions'>
                                <div dangerouslySetInnerHTML={{ __html: window.login_document.policy }} />
                            </Modal.Content>
                        </Modal>
                    </Grid.Column>
                    <Grid.Column>
                        <Segment textAlign='center' basic>
                            <ContactForm color='blue' url='Contattaci'/>
                        </Segment>
                    </Grid.Column>
                    <Grid.Column>
                        <Modal closeIcon 
                            trigger={
                            <Segment textAlign='center' basic>
                                <p className='similar-link'>GDPR (UE) 2016/679</p>
                            </Segment>
                        }>
                            <Modal.Header>GDPR (UE) 2016/679</Modal.Header>
                            <Modal.Content id='gdpr'>
                                <div dangerouslySetInnerHTML={{ __html: window.homepage_document.gdpr }} />
                            </Modal.Content>
                        </Modal>
                    </Grid.Column>
                </Grid>
            </Container>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        userToHome: state.userToHome,
        userHasErrored: state.userHasErrored,
        userIsLoading: state.userIsLoading,
        jwt: state.jwt,
        jwtHasErrored: state.jwtHasErrored,
        jwtIsLoading: state.jwtIsLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchJWT: (url, user, password) => dispatch(jwtFetchData(url, user, password)),
        changeFilter: (mode, value) => dispatch(dispatchFilter(mode, value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(translate('translations')(Login)))
