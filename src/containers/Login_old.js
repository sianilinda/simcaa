import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import LoginForm from '../components/LoginForm'

import { connect } from 'react-redux'
import { jwtFetchData } from '../store/actions/JWTActions'
import { dispatchFilter } from '../store/actions/FilterActions'

class Login extends Component {
    componentDidUpdate(prevProps) {
        if (this.props.userIsLoading === false && this.props.userHasErrored === false && this.props.userToHome === true) {
            this.props.history.push('/init')
        }
    }

    login(user, password) {
        this.props.changeFilter('private')
        this.props.fetchJWT(window.env.GraphQLLogin, user, password)
    }

    render() {
        const message = { text: 'Fill the form to Login into the App', view: this.props.jwtHasErrored, header: 'Welcome to SimCAA'}
        return (
            <LoginForm login={this.login.bind(this)} message={message} loading={this.props.jwtIsLoading}/>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        userToHome: state.userToHome,
        userHasErrored: state.userHasErrored,
        userIsLoading: state.userIsLoading,
        jwt: state.jwt,
        jwtHasErrored: state.jwtHasErrored,
        jwtIsLoading: state.jwtIsLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchJWT: (url, user, password) => dispatch(jwtFetchData(url, user, password)),
        changeFilter: (mode, value) => dispatch(dispatchFilter(mode, value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login))



// WEBPACK FOOTER //
// ./src/containers/Login.js
