import { FETCH_SYMSTYLE_DATA, FETCH_SYMSTYLE_DATA_ERROR, FETCH_SYMSTYLE_DATA_LOADING } from '../constants'

export function symstyleDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_SYMSTYLE_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function symstyleDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_SYMSTYLE_DATA_LOADING:
            return action.isLoading
        default:
            return state
    }
}

export function symstyleData(state = [], action) {
    switch (action.type) {
        case FETCH_SYMSTYLE_DATA:
            return action.symstyle
        default:
            return state
    }
}
