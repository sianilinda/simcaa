import { FETCH_WATERMARK_DATA, FETCH_WATERMARK_DATA_ERROR, FETCH_WATERMARK_DATA_LOADING } from '../constants'

export function watermarkDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_WATERMARK_DATA_ERROR:
            return action.hasErrored
        default:
            return state
    }
}

export function watermarkDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_WATERMARK_DATA_LOADING:
            return action.isLoading
        default:
            return state
    }
}

export function watermarkData(state = [], action) {
    switch (action.type) {
        case FETCH_WATERMARK_DATA:
            return action.watermark
        default:
            return state
    }
}
