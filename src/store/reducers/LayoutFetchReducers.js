import { FETCH_LAYOUT_DATA, FETCH_LAYOUT_DATA_ERROR, FETCH_LAYOUT_DATA_LOADING } from '../constants'

export function layoutDataHasErrored(state = false, action) {
    switch (action.type) {
        case FETCH_LAYOUT_DATA_ERROR:
            return action.hasErrored

        default:
            return state
    }
}

export function layoutDataIsLoading(state = false, action) {
    switch (action.type) {
        case FETCH_LAYOUT_DATA_LOADING:
            return action.isLoading

        default:
            return state
    }
}

export function layoutData(state = [], action) {
    switch (action.type) {
        case FETCH_LAYOUT_DATA:
            return action.layout

        default:
            return state
    }
}
