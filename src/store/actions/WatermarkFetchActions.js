import { FETCH_WATERMARK_DATA, FETCH_WATERMARK_DATA_ERROR, FETCH_WATERMARK_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth } from '../apolloFetchUtils'

export function watermarkDataFetchData(fetchQuery = null, limit = 30, page = 1) {
    return (dispatch, getState) => {

        dispatch(watermarkDataIsLoading(true))
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query fetch_watermark {
                symwater {
                    data {
                        id
                        namewater
                        filewater
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(watermarkDataFetchDataSuccess(data.data.symwater.data))
                dispatch(watermarkDataIsLoading(false))
                if (data.data.symwater.error) {
                    dispatch(watermarkDataHasErrored(true))
                } else {
                    dispatch(watermarkDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(watermarkDataIsLoading(false))
                dispatch(watermarkDataHasErrored(true))
            })
    }
}

export function watermarkDataHasErrored(hasErrored) {
    return {
        type: FETCH_WATERMARK_DATA_ERROR,
        hasErrored
    }
}

export function watermarkDataIsLoading(isLoading) {
    return {
        type: FETCH_WATERMARK_DATA_LOADING,
        isLoading
    }
}

export function watermarkDataFetchDataSuccess(watermark) {
    return {
        type: FETCH_WATERMARK_DATA,
        watermark
    }
}
