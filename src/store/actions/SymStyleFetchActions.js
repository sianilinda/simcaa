import { FETCH_SYMSTYLE_DATA, FETCH_SYMSTYLE_DATA_ERROR, FETCH_SYMSTYLE_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth } from '../apolloFetchUtils'

export function symstyleDataFetchData(fetchQuery, limit = 30, page = 1) {
    return (dispatch, getState) => {
        dispatch(symstyleDataIsLoading(true))
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query FetchStyle {
                simcaa_symstyle(limit: ${limit}, page: ${page}) {
                    data {
                        id
                        paramstyle
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                let imgStyle = [{type: 'imgstyle', value: 0, text: 'random'}]
                let arrayData = data.data.simcaa_symstyle.data
                for (let i=0; i<arrayData.length; i++) {
                    imgStyle.splice(i+1, 0, {type: 'imgstyle',
                                            value: arrayData[i].id,
                                            text: arrayData[i].paramstyle,
                                    })
                }
                dispatch(symstyleDataFetchDataSuccess(imgStyle))
                dispatch(symstyleDataIsLoading(false))

                if (data.data.simcaa_symstyle.error) {
                    dispatch(symstyleDataHasErrored(true))
                } else {
                    dispatch(symstyleDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(symstyleDataIsLoading(false))
                dispatch(symstyleDataHasErrored(true))
            })
    }
}

export function symstyleDataHasErrored(bool) {
    return {
        type: FETCH_SYMSTYLE_DATA_ERROR,
        hasErrored: bool
    }
}

export function symstyleDataIsLoading(bool) {
    return {
        type: FETCH_SYMSTYLE_DATA_LOADING,
        isLoading: bool
    }
}

export function symstyleDataFetchDataSuccess(symstyleData) {
    return {
        type: FETCH_SYMSTYLE_DATA,
        symstyle: symstyleData
    }
}
