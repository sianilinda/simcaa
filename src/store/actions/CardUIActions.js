import { CARDUI_NAVBARCARD, CARDUI_VISIBILITY_IMGALT, CARDUI_SELECTED_PROJECT, CARDUI_SAVE_PROJECT_COMPLETED, CARDUI_SAVE_PROJECT_ERROR, CARDUI_SAVE_PROJECT_LOADING, CARDUI_SEARCH_ISLOADING, CARDUI_COPYPASTE } from '../constants'
import { chapterContentDataFetchDataSuccess, chapterRowsDataFetchDataSuccess } from './ChapterFetchActions'
import { reorderIds, setFocus, handleExpandInput, setExpandAll, prioritySort, setLineSpacing, checkWithImage, refreshToken } from '../functionUtils'
import { apolloFetch, apolloFetchNoAuth } from '../apolloFetchUtils'

export function selectedProjectID(id = -1) {
    return (dispatch) => {
        dispatch({
            type: CARDUI_SELECTED_PROJECT,
            projectId: parseInt(id,10)
        })
    }
}

export function toggleVisibilityImgAlt(value = null) {
    return (dispatch, getState) => {
        if (value) {
            dispatch({
                type: CARDUI_VISIBILITY_IMGALT,
                visibilityImgAlt: value
            })
        } else {
            dispatch({
                type: CARDUI_VISIBILITY_IMGALT,
                visibilityImgAlt: !getState().toggleVisibilityImgAlt
            })
        }
    }
}

// DELETE CARD KB
export function DeleteCard(index) {
    return (dispatch, getState) => {
        let cardArray = getState().chapterContentData.slice()
        // if (index === 0 && cardArray[index + 1] !== undefined && cardArray[index].row !== cardArray[index + 1].row) {
        //     for (let i = index; i <= cardArray[cardArray.length - 1].id; i++) {
        //         if (cardArray[i].row > 0) {
        //             cardArray[i].row = cardArray[i].row - 1
        //         }
        //     }
        //     cardArray.splice(index, 1)
        //     setFocus(index + 1)
        // } else if (cardArray[index - 1] !== undefined && cardArray[index - 1].row !== cardArray[index].row && cardArray[index].lemma !== '') {
        //     for (let i = index; i <= cardArray[cardArray.length - 1].id; i++) {
        //         if (cardArray[i].row > 0) {
        //             cardArray[i].row = cardArray[i].row - 1
        //         }
        //     }
        //     setFocus(index)
        // } else {
        //     console.log(cardArray[index + 1]);
        //     if (cardArray[index + 1] && cardArray[index] !== cardArray[index + 1]) {
        //         console.log('if');
        //         for (let i = index; i <= cardArray[cardArray.length - 1].id; i++) {
        //             if (cardArray[i].row > 0) {
        //                 cardArray[i].row = cardArray[i].row - 1
        //             }
        //         }
        //     }
        //     cardArray.splice(index, 1)
        //     cardArray = reorderIds(cardArray)
        //
        //     // set focus and navcard
        //     if (index > 0) {
        //         setFocus(index - 1)
        //         dispatch(CardUINavbarCard(index - 1))
        //     } else {
        //         setFocus(index)
        //         dispatch(CardUINavbarCard(index))
        //     }
        // }
        if (index === 0 && cardArray[index + 1] && cardArray[index].row !== cardArray[index + 1].row) {
            for (let i = index; i <= cardArray[cardArray.length - 1].id; i++) {
                if (cardArray[i].row > 0) {
                    cardArray[i].row = cardArray[i].row - 1
                }
            }
            cardArray.splice(index, 1)
            dispatch(deleteRow(index))
        } else if (cardArray[index - 1] && cardArray[index].row !== cardArray[index - 1].row && cardArray[index].lemma !== '') {
            dispatch(deleteRow(cardArray[index].row))
            for (let i = index; i <= cardArray[cardArray.length - 1].id; i++) {
                if (cardArray[i].row > 0) {
                    cardArray[i].row = cardArray[i].row - 1
                }
            }
        } else if (cardArray[index - 1] && cardArray[index + 1] && cardArray[index].row !== cardArray[index - 1].row && cardArray[index].row !== cardArray[index + 1].row) {
            dispatch(deleteRow(cardArray[index].row))
            for (let i = index; i <= cardArray[cardArray.length - 1].id; i++) {
                if (cardArray[i].row > 0) {
                    cardArray[i].row = cardArray[i].row - 1
                }
            }
            cardArray.splice(index, 1)
        } else if (cardArray[index - 1] && !cardArray[index + 1] && cardArray[index].row !== cardArray[index - 1].row && cardArray[index].lemma === '') {
            dispatch(deleteRow(cardArray[index].row))
            cardArray.splice(index, 1)
        } else {
            cardArray.splice(index, 1)
        }

        cardArray = reorderIds(cardArray)
        // if (cardArray.length === index) {
        if (index > 0) {
            setFocus(index - 1)
            dispatch(CardUINavbarCard(index - 1))
        } else {
            setFocus(index)
            dispatch(CardUINavbarCard(index))
        }
        dispatch(chapterContentDataFetchDataSuccess(cardArray))
    }
}

export function unlinkCard() {
    return (dispatch, getState) => {
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        let cardArray = getState().chapterContentData.slice()
        let lockStatus = currentCard.codClass === 'Verbo' ? 'lock' : 'unlock'
        let complexLemmaSplit = currentCard.lemma.split(' ')

        let index = getState().projectData.findIndex(x => x.proj_id === parseInt(getState().selectedProjectID,10))
        let data = JSON.parse(getState().projectData[index].proj_profile)

        if (complexLemmaSplit.length === 1) {
            return 0
        }

        for (var i = currentCard.id, j = 0; i < currentCard.id + complexLemmaSplit.length; j++, i++) {
            cardArray.splice(i + 1, 0, {
              id: 0,
              lemma: complexLemmaSplit[j],
              lemmaPrevious: "",
              img: "placeholder.png",
              sinonimi: 0,
              imgAlt: [],
              lock: lockStatus,
              codClass: "Altro",
              complex: "0"
            });
        }
        cardArray.splice(currentCard.id,1)
        cardArray = reorderIds(cardArray)
        let newFocusId = currentCard.id + complexLemmaSplit.length
        dispatch(chapterContentDataFetchDataSuccess(cardArray))
        setTimeout(function() {
            cardArray[newFocusId] ? setFocus(newFocusId) : setFocus(newFocusId - 1)
        }, 300)
        // dispatch(chapterContentDataFetchDataSuccess(cardArray))

        i = currentCard.id
        j = 0
        let indexTimeOut = i
        let indexTimeOutArray = j
        while (i < currentCard.id + complexLemmaSplit.length) {
            setTimeout(() => {
                cardArray = getState().chapterContentData.slice()
                dispatch(setImg(cardArray, indexTimeOut, complexLemmaSplit[indexTimeOutArray], currentCard.row, true, data.imgType, data.imgStyle, data.priorityOrder))
                indexTimeOut++
                indexTimeOutArray++
            }, j*200)
            i++
            j++
        }
    }
}

export function mergeCard(direction, id = null, lemma = null) {
    return (dispatch, getState) => {
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        let cardArray = getState().chapterContentData.slice()
        if (direction === 'right' && cardArray[currentCard.id+1]) {
            cardArray[currentCard.id+1].lemma = currentCard.lemma + ' ' + cardArray[currentCard.id+1].lemma
            cardArray[currentCard.id+1].lock = 'lock'
            setTimeout(() => {
                // TODO: verificare se non serve
                // self.handleExpandInput(cardArray[currentCard.id+1])
                handleExpandInput(cardArray[currentCard.id].id)
            }, 0)
            if (cardArray[currentCard.id].row !== cardArray[currentCard.id + 1].row && (currentCard.id === 0 || (cardArray[currentCard.id - 1] && cardArray[currentCard.id].row !== cardArray[currentCard.id - 1].row))) {
                dispatch(deleteRow(cardArray[currentCard.id].row))
            }
            cardArray.splice(currentCard.id, 1)
            dispatch(CardUINavbarCard(cardArray[currentCard.id]))
        }
        else if (direction === 'left' && cardArray[currentCard.id-1]) {
            cardArray[currentCard.id-1].lemma += ' ' + currentCard.lemma
            cardArray[currentCard.id-1].lock = 'lock'
            setTimeout(() => {
                handleExpandInput(cardArray[currentCard.id-1].id)
            }, 0)
            if (cardArray[currentCard.id].row !== cardArray[currentCard.id - 1].row && (currentCard.id === cardArray.length - 1 || (cardArray[currentCard.id + 1] && cardArray[currentCard.id].row !== cardArray[currentCard.id + 1].row))) {
                dispatch(deleteRow(cardArray[currentCard.id].row))
            }
            cardArray.splice(currentCard.id, 1)
            dispatch(CardUINavbarCard(cardArray[currentCard.id-1]))
        }
        cardArray = reorderIds(cardArray)
        dispatch(chapterContentDataFetchDataSuccess(cardArray))

        if (id !== null && lemma) {
            let index = getState().projectData.findIndex(x => x.proj_id === parseInt(getState().selectedProjectID,10))
            let data = JSON.parse(getState().projectData[index].proj_profile)
            dispatch(setImg(cardArray, id, lemma, currentCard.row, true, data.imgType, data.imgStyle, data.priorityOrder))
        }
        dispatch(handleCheckRows())
        dispatch(adjustCard())
    }
}

export function AddNewBlankCard(index, row) {
    return (dispatch, getState) => {
        let cardArray = getState().chapterContentData.slice()
        let newIndex = index < 0 ? 0 : index
        cardArray.splice(newIndex, 0, {id: index,
            lemma: '',
            lemmaPrevious: '',
            img: 'placeholder.png',
            sinonimi: 0,
            imgAlt: [],
            lock: 'unlock',
            codClass: 'Altro',
            complex: '0',
            row
        })
        // console.log(cardArray[index-1].id);
        cardArray = reorderIds(cardArray)
        dispatch(chapterContentDataFetchDataSuccess(cardArray))
        dispatch(adjustCard())
    }
}

export function CardUINavbarCard(value) {
    if (typeof value === 'number') {
        return (dispatch, getState) => {
            let cards = getState().chapterContentData.slice()
            dispatch({
                type: CARDUI_NAVBARCARD,
                navbarCard: cards[value]
            })
        }
    } else {
        return (dispatch, getState) => {
            dispatch({
                type: CARDUI_NAVBARCARD,
                navbarCard: value
            })
        }
    }
}

export function setImg(cardArray = null, index, lemma, row, NoAuth = null, imgType, imgStyle, priorityOrder) {
    return (dispatch, getState) => {
        dispatch(CardUIsearchIsLoading(true))

        if (!cardArray || cardArray === null) {
            cardArray = getState().chapterContentData.slice()
        }

        // Create param for preload query
        let projId = getState().selectedProjectID
        let indexProjArray = getState().projectData.findIndex((item) => projId === item.proj_id)
        let queryParams = JSON.parse(getState().projectData[indexProjArray].proj_profile).queryParams
        let param = ''
        param += queryParams.preload_team ? '1' : '0'
        param += queryParams.preload_community ? '1' : '0'
        let userId = queryParams.preload_private ? getState().user.id : getState().user.id * -1

        // Assemble the query
        lemma = lemma.toLowerCase()
        let topQuery = 'query FatchLemma {'
        let bottomQuery = '}'
        let query_view = `
        query_view (voice_master: "${lemma}", limit: 100){
            data {
                voice_master
                voice_human
                symbol_sign
                idclass
                lexical_expr
                imgcolor
                idstyle
            }
        }
        `
        let preload_headword = `
        preload_headword(voice_master: "${lemma}", created_by: ${userId} , param: "${param}", limit: 100) {
            data {
                id
                voice_human
                with_image
                lexical_expr
                idclass
                symbol_sign
                imgcolor
                idstyle
            }
        }
        `
        let query = queryParams.global ? topQuery + query_view + preload_headword + bottomQuery : topQuery + preload_headword + bottomQuery

        let catchVar = (error) => {
            cardArray[index].lemma = lemma
            cardArray[index].sinonimi = 0
            cardArray[index].img = 'placeholder.png'
            cardArray[index].row = row
            dispatch(chapterContentDataFetchDataSuccess(cardArray))
            dispatch(CardUIsearchIsLoading(false))
        }

        let thenVar = (data) => {
            dispatch(CardUIsearchIsLoading(false))

            let newDataSorting = data.data.query_view ? data.data.query_view.data : []
            let imgAltQuery2 = data.data.preload_headword.data

            // Ordina l'cardArray
            let completeOrderedArray
            if (newDataSorting.length > 0) {
                completeOrderedArray = prioritySort(newDataSorting, lemma, imgType, imgStyle, priorityOrder)
            } else {
                completeOrderedArray = prioritySort(imgAltQuery2, lemma, imgType, imgStyle, priorityOrder)
            }

            if (typeof completeOrderedArray !== 'undefined' && completeOrderedArray.length > 0) {
                // TODO: Cambiare una volta aggiunto il multilingua
                // Identifica la classe del lemma
                switch (completeOrderedArray[0].idclass) {
                    case 2:
                    cardArray[index].codClass = 'Aggettivo'
                    break
                    case 3:
                    cardArray[index].codClass = 'Articolo'
                    break
                    case 4:
                    cardArray[index].codClass = 'Avverbio'
                    break
                    case 5:
                    cardArray[index].codClass = 'Congiunzione'
                    break
                    case 10:
                    cardArray[index].codClass = 'Interiezione'
                    break
                    case 14:
                    cardArray[index].codClass = 'Pronome'
                    break
                    case 16:
                    cardArray[index].codClass = 'Preposizione'
                    break
                    case 17:
                    cardArray[index].codClass = 'Sostantivo'
                    break
                    case 20:
                    cardArray[index].codClass = 'Verbo'
                    break
                    case 27:
                    cardArray[index].codClass = 'Altro'
                    break
                    default:
                    break
                }
                cardArray[index].lemmaPrevious = cardArray[index].lemma
                cardArray[index].sinonimi = completeOrderedArray.length
                cardArray[index].imgAlt = []
                cardArray[index].complex = completeOrderedArray[0].lexical_expr
                cardArray[index].row = row
                cardArray[index].custom = checkWithImage(completeOrderedArray[0])

                if (completeOrderedArray[0].voice_human === lemma) {
                    cardArray[index].img = completeOrderedArray[0].symbol_sign
                    cardArray[index].lemma = completeOrderedArray[0].voice_human
                } else {
                    dispatch(toggleVisibilityImgAlt(true))
                }

                for (let i=0; i<completeOrderedArray.length; i++) {
                    cardArray[index].imgAlt.splice(i, 0, {voice_human: completeOrderedArray[i].voice_human,
                        voice_master: completeOrderedArray[i].voice_master,
                        img: completeOrderedArray[i].symbol_sign,
                        complex: completeOrderedArray[i].lexical_expr,
                        custom: checkWithImage(completeOrderedArray[i])
                    })
                }

                // accodo le img della seconda query
                // TODO: vedere meglio con ordinamento
                if (newDataSorting.length > 0 && imgAltQuery2.length > 0) {
                    for (let i = 0; i < imgAltQuery2.length; i++) {
                        cardArray[index].imgAlt.splice(i + cardArray[index].imgAlt.length, 0,
                            {voice_human: imgAltQuery2[i].voice_human,
                                voice_master: imgAltQuery2[i].voice_human,
                                img: imgAltQuery2[i].symbol_sign,
                                complex: imgAltQuery2[i].lexical_expr,
                                custom: checkWithImage(imgAltQuery2[i]),
                            })
                        }
                    }
            } else {
                cardArray[index].lemma = lemma
                cardArray[index].sinonimi = 0
                cardArray[index].img = 'placeholder.png'
                cardArray[index].row = row
            }
            let finalCurrentCard = Object.assign({}, cardArray[index])
            let updatedCardArray = getState().chapterContentData.slice()
            updatedCardArray[index] = Object.assign({}, finalCurrentCard)

            dispatch(setComplexVerbs(cardArray[index]))
            dispatch(adjustCard())
            dispatch(handleCheckRows(updatedCardArray))
            dispatch(CardUINavbarCard(index))
        }

        if (NoAuth) {
            apolloFetchNoAuth({ query })
                .then(thenVar)
                .catch(catchVar)
        } else {
            apolloFetch({ query })
                .then(thenVar)
                .catch(catchVar)
        }
    }
}

export function setComplexVerbs() {
    return (dispatch, getState) => {
        setTimeout(function() {
            let currentCard = Object.assign({}, getState().CardUINavbarCard)
            let localCards = getState().chapterContentData.slice()
            let previousId = currentCard.id - 1
            if (previousId > -1 && localCards[previousId].codClass === 'Verbo' && currentCard.codClass === 'Verbo' && localCards[previousId].lock === 'unlock') {
                let complexLemma = localCards[previousId].lemma + ' ' + currentCard.lemma
                let query = `
                query CheckComplex {
                    query_view(voice_master: "${complexLemma}") {
                        data {
                            idheadword
                            voice_human
                        }
                    }
                }
                `
                apolloFetch({ query })
                    .then((data) => {
                        if (data.data.query_view.data.length > 0) {
                            dispatch(CardUINavbarCard(localCards[currentCard.id]))
                            dispatch(mergeCard('left', previousId, data.data.query_view.data[0].voice_human))
                            setFocus(currentCard.id)
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            }
        }, 100)
    }
}

export function setNewImg(event, imgAlt) {
    return (dispatch, getState) => {
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        let localCards = getState().chapterContentData.slice()
        localCards[currentCard.id].img = imgAlt.img
        localCards[currentCard.id].lemma = imgAlt.voice_human
        localCards[currentCard.id].complex = imgAlt.complex
        localCards[currentCard.id].custom = imgAlt.custom
        let complexLemmaSplit = localCards[currentCard.id].lemma.split(' ')
        let cont = 0
        if (localCards[currentCard.id + complexLemmaSplit.length - 1]) {
            for (let i = 1; i < complexLemmaSplit.length; i++) {
                currentCard.id++
                if (localCards[currentCard.id].lemma === complexLemmaSplit[i]) {
                    cont++
                }
            }
        }
        currentCard = Object.assign({}, getState().CardUINavbarCard)
        if (cont === complexLemmaSplit.length - 1) {
            localCards.splice(currentCard.id + 1, cont)
        }
        localCards = reorderIds(localCards)
        dispatch(CardUINavbarCard(currentCard.id))
        dispatch(adjustCard())
        dispatch(chapterContentDataFetchDataSuccess(localCards))
        dispatch(toggleVisibilityImgAlt(false))
    }
}

export function blockCard() {
    return (dispatch, getState) => {
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        let localCards = getState().chapterContentData.slice()
        localCards[currentCard.id].lock === 'lock' ? localCards[currentCard.id].lock = 'unlock' : localCards[currentCard.id].lock = 'lock'
        localCards[currentCard.id].lemma = localCards[currentCard.id].lemma.trim()
        dispatch(chapterContentDataFetchDataSuccess(localCards))
        dispatch(CardUINavbarCard(currentCard.id))
    }
}

export function copyCard() {
    return (dispatch, getState) => {
        let card = Object.assign({}, getState().CardUINavbarCard)
        dispatch({
            type: CARDUI_COPYPASTE,
            card
        })
    }
}

export function pasteCard() {
    return (dispatch, getState) => {
        let navbarCard = Object.assign({}, getState().CardUINavbarCard)
        let cardToPaste = Object.assign({}, getState().CardUIcopyPaste)
        if (cardToPaste && Object.keys(cardToPaste).length > 0) {
            let localCards = getState().chapterContentData.slice()
            cardToPaste.row = navbarCard.row
            localCards.splice(navbarCard.id + 1, 0, cardToPaste)
            localCards = reorderIds(localCards)
            dispatch(chapterContentDataFetchDataSuccess(localCards))
            dispatch(adjustCard())
        }
    }
}

export function searchLemma() {
    return (dispatch, getState) => {
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        let localCards = getState().chapterContentData.slice()

        let index = getState().projectData.findIndex(x => x.proj_id === parseInt(getState().selectedProjectID,10))
        let data = JSON.parse(getState().projectData[index].proj_profile)

        dispatch(setImg(localCards, currentCard.id, currentCard.lemma, currentCard.row, true, data.imgType, data.imgStyle, data.priorityOrder))
    }
}

// DELETE CARD BUTTON
export function deleteCard() {
    return (dispatch, getState) => {
        let localCards = getState().chapterContentData.slice()
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        let index = localCards[currentCard.id].id
        if (localCards.length > 1) {
            if ((localCards[index - 1] && localCards[index - 1].row !== localCards[index].row) && (localCards[index + 1] && localCards[index + 1].row !== localCards[index].row)) {
                dispatch(deleteRow(localCards[index].row))
            }
            if ((localCards[index - 1] && localCards[index - 1].row !== localCards[index].row) && !localCards[index + 1]) {
                dispatch(deleteRow(localCards[index].row))
            }
            if (!localCards[index - 1] && (localCards[index + 1] && localCards[index + 1].row !== localCards[index].row)) {
                dispatch(deleteRow(localCards[index].row))
            }
            localCards.splice(index, 1)
            localCards = reorderIds(localCards)
            index > 0 ? setFocus(index-1) : setFocus(index)
            index > 0 ? dispatch(CardUINavbarCard(index-1)) : dispatch(CardUINavbarCard(index))
        }
        dispatch(adjustCard())
        dispatch(handleCheckRows(localCards))
    }
}

export function jumpRow() {
    return (dispatch, getState) => {
        let localCards = getState().chapterContentData.slice()
        let currentCard = Object.assign({}, getState().CardUINavbarCard)
        if (currentCard.id === localCards.length - 1 && currentCard.lemma !== '') {
            dispatch(AddNewBlankCard(currentCard.id + 1, currentCard.row + 1))
            dispatch(addNewRow(currentCard.row + 1, 'new_line'))
        } else if (currentCard.lemma !== '' && !(localCards[currentCard.id + 1].lemma === '' && localCards[currentCard.id].row !== localCards[currentCard.id + 1].row)) {
            if (localCards[currentCard.id].row !== localCards[currentCard.id + 1].row && localCards[currentCard.id + 1].lemma !== '') {
                dispatch(AddNewBlankCard(currentCard.id + 1, currentCard.row))
            }
            localCards = getState().chapterContentData.slice()
     
            for (let i = currentCard.id + 1; i < localCards.length; i++) {
                localCards[i].row++
            }

            dispatch(adjustCard())
            dispatch(chapterContentDataFetchDataSuccess(localCards))
            dispatch(addNewRow(currentCard.row + 1, 'new_line'))
        }
    }
}

export function handleCheckRows(cardArray = null) {
    return (dispatch, getState) => {
        if (cardArray === null) {
            cardArray = getState().chapterContentData.slice()
        }
        for (let i = 0; i < cardArray.length - 1; i++) {
            if (cardArray[i + 1].row - cardArray[i].row > 1) {
                for (let j = i + 1; j < cardArray.length; j++) {
                    if (cardArray[j].row > 0) {
                        cardArray[j].row--
                    }
                }
            }
        }
        if (cardArray[0].row !== 0) {
            let delta = 0 - cardArray[0].row
            for (let i = 0; i < cardArray.length; i++) {
                cardArray[i].row += delta
            }
        }
        dispatch(chapterContentDataFetchDataSuccess(cardArray))
    }
}

// Delete void card (no lemma)
export function clearCard(cardArray = null) {
    return (dispatch, getState) => {
        if (cardArray === null) {
            cardArray = getState().chapterContentData.slice()
        }

        let idToDelete = [],
            rowToDelete = []
        for (let i = 0; i < cardArray.length; i++) {
            if (!cardArray[i].lemma) {
                if ((cardArray[i - 1] && cardArray[i - 1].row !== cardArray[i].row) && (cardArray[i + 1] && cardArray[i + 1].row !== cardArray[i].row)) {
                    rowToDelete.push(cardArray[i].row)
                }
                if ((cardArray[i - 1] && cardArray[i - 1].row !== cardArray[i].row) && !cardArray[i + 1]) {
                    rowToDelete.push(cardArray[i].row)
                }
                if (!cardArray[i - 1] && (cardArray[i + 1] && cardArray[i + 1].row !== cardArray[i].row)) {
                    rowToDelete.push(cardArray[i].row)
                }
                idToDelete.push(i)
            }
        }

        for (let i = idToDelete.length - 1; i >= 0; i--) {
            cardArray.splice(idToDelete[i], 1) 
        }

        for (let i = rowToDelete.length - 1; i >= 0; i--) {
            dispatch(deleteRow(rowToDelete[i]))
        }

        cardArray = reorderIds(cardArray)
        dispatch(handleCheckRows(cardArray))
        dispatch(adjustCard())
    }
}

export function saveProject(chapterId, doRefresh = null) {
    return (dispatch, getState) => {
        dispatch(CardUIsaveIsCompleted(false))
        dispatch(CardUIsaveHasErrored(false))
        dispatch(CardUIsaveIsLoading(true))
        dispatch(clearCard())

        let localProject = { "id":  chapterId, "chapt_content": "", "chapt_row": ""}
        localProject.chapt_content = JSON.stringify(getState().chapterContentData.slice())
        localProject.chapt_row = JSON.stringify(getState().chapterRowsData.slice())
        let url = window.env.RestApiCard
        var data = JSON.stringify(localProject)
        var xhr = new XMLHttpRequest()
        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    dispatch(CardUIsaveIsCompleted(true))
                    dispatch(CardUIsaveHasErrored(false))
                    dispatch(CardUIsaveIsLoading(false))
                } else {
                    dispatch(CardUIsaveIsCompleted(false))
                    dispatch(CardUIsaveHasErrored(true))
                    dispatch(CardUIsaveIsLoading(false))
                }
            }
        })
        xhr.open("POST", url)
        xhr.setRequestHeader("content-type", "application/json")
        xhr.send(data)

        // Refresh the token for the current user
        if (doRefresh === null) {
            refreshToken()
        }
    }
}

export function adjustCard() {
    return (dispatch, getState) => {
        setExpandAll(getState().chapterContentData.slice())
        let indexProject = getState().projectData.findIndex(x => x.proj_id === getState().selectedProjectID)
        let margins = JSON.parse(getState().projectData[indexProject].proj_profile).layout.layout_margins
        setLineSpacing(margins)
    }
}

export function addNewRow(row, type) {
    return (dispatch, getState) => {
        let rowsArray = getState().chapterRowsData.slice()
        rowsArray.splice(row, 0, {times: 1, type, row: 0, page: false})
        dispatch(chapterRowsDataFetchDataSuccess(rowsArray))
    }
}

export function editRow(row, action) {
    return (dispatch, getState) => {
        let rowsArray = getState().chapterRowsData.slice()
        if (action === 'add' && row !== 0) {
            rowsArray[row].times++
        } else if (action === 'subtract' && rowsArray[row].times > 1 && row !== 0) {
            rowsArray[row].times--
        } else if (action === 'page' && row > 0) {
            rowsArray[row].page = !rowsArray[row].page
        }
        dispatch(chapterRowsDataFetchDataSuccess(rowsArray))
    }
}

export function deleteRow(index) {
    return (dispatch, getState) => {
        let rowsArray = getState().chapterRowsData.slice()
        if (rowsArray.length > 1) {
            if (rowsArray[index + 1] && rowsArray[index].page) {
                rowsArray[index + 1].page = true
            }
            rowsArray.splice(index, 1)
            dispatch(chapterRowsDataFetchDataSuccess(rowsArray))
        }
    }
}

export function handleWatermark(watermark, reset = false) {
    return (dispatch, getState) => {
        let localCards = JSON.parse(JSON.stringify(getState().chapterContentData))
        let index = getState().CardUINavbarCard.id

        // TODO: DELETE WHEN OLD CHAPTER COMPLETELY DELETE
        if (!localCards[index].watermark) {
            localCards[index].watermark = {
                'negazione': false,
                'plurale': false,
                'passato': false,
                'futuro': false,
            }
        }

        if (reset === false) {
            if (watermark === 'futuro') {
                localCards[index].watermark['passato'] = false
            } else if (watermark === 'passato') {
                localCards[index].watermark['futuro'] = false
            }
    
            localCards[index].watermark[watermark] = !localCards[index].watermark[watermark]
        } else {
            Object.keys(localCards[index].watermark).forEach((key) => localCards[index].watermark[key] = false)
        }

        dispatch(CardUINavbarCard(localCards[index]))
        dispatch(chapterContentDataFetchDataSuccess(localCards))
    }
}

export function CardUIsaveIsCompleted(isCompleted) {
    return (dispatch) => {
        dispatch({
            type: CARDUI_SAVE_PROJECT_COMPLETED,
            isCompleted
        })
    }
}

export function CardUIsaveHasErrored(hasErrored) {
    return (dispatch) => {
        dispatch({
            type: CARDUI_SAVE_PROJECT_ERROR,
            hasErrored
        })
    }
}

export function CardUIsaveIsLoading(isLoading) {
    return (dispatch) => {
        dispatch({
            type: CARDUI_SAVE_PROJECT_LOADING,
            isLoading
        })
    }
}

export function CardUIsearchIsLoading(isLoading) {
    return (dispatch) => {
        dispatch({
            type: CARDUI_SEARCH_ISLOADING,
            isLoading
        })
    }
}

export function CardUIcopyPaste(card) {
    return (dispatch) => {
        dispatch({
            type: CARDUI_COPYPASTE,
            card
        })
    }
}
