import { FETCH_STYLE_DATA, FETCH_STYLE_DATA_ERROR, FETCH_STYLE_DATA_LOADING } from '../constants'
import { apolloFetchNoAuth } from '../apolloFetchUtils'

export function styleDataFetchData(fetchQuery, limit = 30, page = 1) {
    return (dispatch, getState) => {
        dispatch(styleDataIsLoading(true))
        let query

        if (fetchQuery) {
            query = fetchQuery
        } else {
            query = `
            query allStyles {
                style(limit: ${limit}, page: ${page}) {
                    data {
                        id
                        descstyle
                    }
                }
            }
            `
        }

        apolloFetchNoAuth({ query })
            .then((data) => {
                dispatch(styleDataFetchDataSuccess(data.data.style.data))
                dispatch(styleDataIsLoading(false))
                if (data.data.style.error) {
                    dispatch(styleDataHasErrored(true))
                } else {
                    dispatch(styleDataHasErrored(false))
                }
            })
            .catch((error) => {
                dispatch(styleDataIsLoading(false))
                dispatch(styleDataHasErrored(true))
            })
    }
}

export function styleDataHasErrored(bool) {
    return {
        type: FETCH_STYLE_DATA_ERROR,
        hasErrored: bool
    }
}

export function styleDataIsLoading(bool) {
    return {
        type: FETCH_STYLE_DATA_LOADING,
        isLoading: bool
    }
}

export function styleDataFetchDataSuccess(styleData) {
    return {
        type: FETCH_STYLE_DATA,
        style: styleData
    }
}
