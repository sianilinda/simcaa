import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Form, Message } from 'semantic-ui-react'
import { translate } from 'react-i18next'

class NewChapterForm extends Component {
    constructor(props) {
        super(props)
        this.state= {
            modalVisible: false,
            errorMessage: true,
            newChaptherTitle: '',
            iconColor: 'black',
        }
    }

    handleOpenCloseModal() {
        if (this.props.edit) {
            this.setState({modalVisible: !this.state.modalVisible, newChaptherTitle: this.props.chaptTitle, errorMessage: true})
        } else {
            this.setState({modalVisible: !this.state.modalVisible, newChaptherTitle: '', errorMessage: true})
        }
    }

    handleFormChange(e) {
        this.setState({newChaptherTitle: e.target.value})
    }

    handleForm() {
        if(this.state.newChaptherTitle === '') {
            this.setState({errorMessage: false})
        }
        else if(!this.props.edit) {
            this.props.createChapter(this.state.newChaptherTitle)
            this.setState({errorMessage: true, modalVisible: !this.state.modalVisible})
        } else {
            let query = `
            mutation ChangeTitle {
                updateCaaChapter(id: ${this.props.chaptID}, chapt_title: "${this.state.newChaptherTitle}") {
                    id
                }
            }
            `
            this.props.updateChapter(query, this.props.projID)
            this.setState({errorMessage: true, modalVisible: !this.state.modalVisible})
        }
    }

    onMouseOverIcon() {
        let newColor = this.state.iconColor === 'black' ? 'green' : 'black'
        this.setState({iconColor: newColor})
    }

    render() {
        const { t } = this.props

        let iconModal = <Icon name={this.props.nameIcon}
                className={this.props.className}
                size={this.props.sizeIcon}
                color={this.state.iconColor}
                onMouseOver={this.props.overIcon ? this.onMouseOverIcon.bind(this) : null}
                onMouseOut={this.props.overIcon ? this.onMouseOverIcon.bind(this) : null}
                onClick={this.handleOpenCloseModal.bind(this)}
                style={this.props.iconStyle}
            />

        return (
            <div style={this.props.divStyle}>
                <Modal trigger={iconModal} closeOnDimmerClick={false} open={this.state.modalVisible}>
                    <Modal.Header>{t("PRJ_FRM_HEADER")}</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Form.Field required>
                                <label>{t("PRJ_FRM_TITLE")}</label>
                                <input placeholder={t("PRJ_FRM_TITLE")}
                                    value={this.state.newChaptherTitle}
                                    onChange={this.handleFormChange.bind(this)}
                                    maxLength={55}
                                />
                            </Form.Field>
                        </Form>
                        <Message
                            hidden={this.state.errorMessage}
                            error
                            header={t("PRJ_FRM_ERROR_HEADER")}
                            content={t("ERR_UPDATE_CHAPTER")}
                        />
                    </Modal.Content>
                    <Modal.Actions>
                        <Button.Group>
                            <Button color='green' onClick={this.handleForm.bind(this)}>
                                {this.props.edit ? t("PRJ_BTN_UPDATE") : t("PRJ_BTN_CREATE")}
                            </Button>
                            <Button.Or />
                            <Button color='red' onClick={this.handleOpenCloseModal.bind(this)}>
                                {t("PRJ_BTN_CLOSE")}
                            </Button>
                        </Button.Group>
                    </Modal.Actions>
                </Modal>
            </div>
        )
    }
}

NewChapterForm.propTypes = {
    createChapter: PropTypes.func,
    updateChapter: PropTypes.func,
    className: PropTypes.string,
    iconStyle: PropTypes.object,
    divStyle: PropTypes.object,
    nameIcon: PropTypes.string.isRequired,
    sizeIcon: PropTypes.string.isRequired,
    overIcon: PropTypes.bool,
    edit: PropTypes.bool,
    chaptTitle: PropTypes.string,
    chaptID: PropTypes.number,
    projID: PropTypes.number,
}

NewChapterForm.defaultProps = {
    overIcon: true,
    edit: false,
}

export default translate('translations')(NewChapterForm)
