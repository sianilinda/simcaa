import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Icon, Button, Card, Confirm, Segment } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import NewChapterForm from './NewChapterForm'

class ChapterUI extends Component {
    constructor(props) {
        super(props)
        this.state= {
            openConfirm: false,
        }
    }

    // Handle open/close of the delete confirm
    handleOpenCloseConfirm(id, event) {
        this.setState({openConfirm: !this.state.openConfirm})
    }

    // Call the props to delete the current chapter
    handleConfirm() {
        this.props.deleteChapter(this.props.chapter.id)
    }

    // Force unlock of the chapter
    forceUnlockChapter(id, event) {
        this.props.forceUnlockChapter(id)
    }

    // Duplicate the chapter
    duplicateChapter(id, e) {
        this.props.duplicateChapter(id)
    }

    render() {
        const { t } = this.props
        
        let ifBlocked = this.props.chapter.chapt_user_block === 0 ? false : true
        let isProjBlockedOrNotOwner = this.props.projectOwner !== this.props.user.id && (this.props.projectBlocked === 1 || ifBlocked === true) ? true : false

        let contentButton
        if (ifBlocked === true && (this.props.chapter.chapt_user_block === this.props.user.id || this.props.user.role_id === 1 || this.props.projectOwner === this.props.user.id)) {
            ifBlocked = false
            contentButton = <Button color='green'
                disabled={ifBlocked}
                onClick={this.forceUnlockChapter.bind(this, this.props.chapter.id)}
            >
                <Icon name='lock' /> Unlock
            </Button>
        } else {
            contentButton = <Button color='green'
                as={Link}
                disabled={isProjBlockedOrNotOwner}
                to={'/basic/edit/' + this.props.projectId + '/' + this.props.chapter.id}
            >
                {t("PRJ_BTN_EDIT")}
            </Button>
        }
        
        return (
            <Card style={{'textAlign': 'center', 'width': 'auto'}}>
                <Card.Content extra className='extra-alpha-1'>
                    <Icon name='clone' size='big'
                        disabled={isProjBlockedOrNotOwner}
                        className='float-left icon-pointer'
                        onClick={this.duplicateChapter.bind(this, this.props.chapter.id)}
                    />
                    <Segment basic disabled={isProjBlockedOrNotOwner} style={{display: 'inline'}}>
                        <NewChapterForm edit
                            updateChapter={this.props.updateChapter.bind(this)}
                            chaptTitle={this.props.chapter.chapt_title}
                            chaptID={this.props.chapter.id}
                            projID={this.props.projectId}
                            nameIcon='edit'
                            sizeIcon='big'
                            className='icon-pointer'
                            overIcon={false}
                            divStyle={{'display': 'inline-block', 'float': 'left'}}
                        />
                    </Segment>
                    {'Chapt id: ' + this.props.chapter.id}
                </Card.Content>
                <Card.Content>
                    {this.props.chapter.chapt_title}
                </Card.Content>
                <Card.Content extra>
                    <Button.Group>
                        {contentButton}
                        <Button.Or />
                        <Button color='blue'
                            as={Link}
                            to={'/basic/view/' + this.props.projectId + '/' + this.props.chapter.id}
                        >
                            {t("PRJ_BTN_VIEW")}
                        </Button>
                        <Button.Or />
                        <Button color='red'
                            disabled={this.props.projectOwner === this.props.user.id && this.props.chapter.chapt_user_block === 0 ? false : true}
                            onClick={this.handleOpenCloseConfirm.bind(this)}
                        >
                            {t("PRJ_BTN_DELETE")}
                        </Button>
                    </Button.Group>
                </Card.Content>
                <Confirm
                    open={this.state.openConfirm}
                    header={t("DELETE_CNF_H")}
                    content={t("DELETE_CNF_P")}
                    onCancel={this.handleOpenCloseConfirm.bind(this)}
                    onConfirm={this.handleConfirm.bind(this)}
                />
            </Card>
        )
    }
}

ChapterUI.propTypes = {
    forceUnlockChapter: PropTypes.func.isRequired,
    deleteChapter: PropTypes.func.isRequired,
    duplicateChapter: PropTypes.func.isRequired,
    chapter: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    projectId: PropTypes.number.isRequired,
    projectOwner: PropTypes.number.isRequired,
    projectBlocked: PropTypes.number,
}

export default translate('translations')(ChapterUI)
