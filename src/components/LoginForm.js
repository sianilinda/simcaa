import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Segment, Grid, Message, Divider, Container } from 'semantic-ui-react'

import ContactForm from './ContactForm'

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            password: '',
        }
    }

    changeFormEmail(event, input) {
        this.setState({user: input.value})
    }

    changeFormPassword(event, input) {
        this.setState({password: input.value})
    }

    callLogin() {
        this.props.login(this.state.user, this.state.password)
    }

    render() {
        let disable
        if (this.props.message.view) {
            disable = false
        } else if (!this.props.message.view && this.props.loading) {
            disable = true
        }

        // TODO: DELETE WHEN MESSAGE NOT NEEDED
        let style
        if (this.props.noMessage) {
            style = {'display': 'none'}
        } else {
            style = {'maxWidth': '50%', 'left': '50%', 'transform': 'translateX(-50%)'}
        }

        if(window.env.maintenanceMode) {
            return (
                <Message negative>
                    <Message.Content>
                     <div dangerouslySetInnerHTML={{ __html: window.env.maintenanceMessage }} />
                    </Message.Content>
                </Message>
            )
        } else {
            return (
                <div>
                    <Grid
                        textAlign='center'
                        style={{ height: '100%' }}
                        verticalAlign='middle'
                    >
                        <Grid.Column style={{ maxWidth: 450 }}>
                            <Form size='large' id='login' error={this.props.message.view}>
                                <Segment raised color='green'>
                                    <Message error={this.props.message.view}>
                                        <Message.Header>{this.props.message.header}</Message.Header>
                                        {this.props.message.text}
                                    </Message>
                                    <Form.Input
                                        fluid
                                        icon='user'
                                        iconPosition='left'
                                        placeholder='Username'
                                        value={this.state.user}
                                        onChange={this.changeFormEmail.bind(this)}
                                    />
                                    <Form.Input
                                        fluid
                                        icon='lock'
                                        iconPosition='left'
                                        placeholder='Password'
                                        type='password'
                                        value={this.state.password}
                                        onChange={this.changeFormPassword.bind(this)}
                                    />
                                    <Button type='submit' fluid color='blue'
                                        disabled={disable}
                                        loading={disable}
                                        onClick={this.callLogin.bind(this)}
                                    >
                                        Login
                                    </Button>
                                    <Divider horizontal>
                                        or
                                    </Divider>
                                    <ContactForm type='reset_password' />
                                </Segment>
                            </Form>
                        </Grid.Column>
                    </Grid>
                </div>
            )
        }
    }

}

LoginForm.propTypes = {
    login: PropTypes.func.isRequired,
    message: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
}

export default LoginForm
