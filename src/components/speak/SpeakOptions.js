import React, { Component } from 'react'
import { Dropdown, Form,  Modal, Button, Radio } from 'semantic-ui-react'
import { translate } from 'react-i18next'

// Import piccolo CSS per lo style degli slider
import '../../css/slider.css'

class SpeakOptions extends Component {
    constructor(props) {
        super(props)
        this.state =  {
            voci: {},
            selectedVoice: "Italian Female",
            volume: 0.5,
            rate: 0.75,
            pitch: 1,
            scansione: 0,
            evidenziatore: 'card',
        }
    }

    // Carica l'elenco delle voci
    componentWillMount() {
        // let voci = window.responsiveVoice.getVoices()
        // let opzionivoci=[]
        // for (let i = 0; i < voci.length; i++) {
        //     opzionivoci.push({text:voci[i].name,value: voci[i].name})[i]
        // }
        // this.setState({voci: opzionivoci})
        this.setState({voci: [{text: 'Italian Female', value: 'Italian Female'}, {text: 'Italian Male', value: 'Italian Male'}]})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({volume: nextProps.volume, rate: nextProps.rate,
                    pitch: nextProps.pitch, scansione: nextProps.scansione,
                    evidenziatore: nextProps.evidenziatore, selectedVoice: nextProps.voice})
    }

    // handle change del volume
    changeVolume(e) {
        this.setState({volume: e.target.value})
    }

    // Handle change della velocità
    changeRate(e) {
        this.setState({rate: e.target.value})
    }

    // Handle change del pitch
    changePitch(e) {
        this.setState({pitch: e.target.value})
    }

    // Handle change della scansione
    changeScansione(e) {
        this.setState({scansione: e.target.value})
    }

    // Handle change delle voci
    changeVoice(e,data) {
        this.setState({selectedVoice: data.value})
    }

    // Handle change evidenziatore
    changeEvidenziatore(e, data) {
        this.setState({evidenziatore: data.value})
    }

    // Legge per provare le impostazioni
    playTest(e) {
        window.responsiveVoice.speak("Questa è una prova di lettura", this.state.selectedVoice,
            {volume: this.state.volume, rate:this.state.rate, pitch:this.state.pitch})
    }

    // Salva e chiude la modale
    saveOptions() {
        this.props.saveOptions(this.state.volume,
                                this.state.rate,
                                this.state.pitch,
                                this.state.scansione,
                                this.state.evidenziatore,
                                this.state.selectedVoice)
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        return (
            <Modal open={this.props.open}>
                <Modal.Header>{t("HEAD_TTS_TEXT")}</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Field>
                            <label>{t("TTS_FRM_VOLUME")}</label>
                            <input type="range" className = "slider" id="volumeSlider" min={0} max={1}
                                value={this.state.volume} step={0.0001} onChange={this.changeVolume.bind(this)}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("TTS_FRM_PITCH")}</label>
                            <input type="range" className = "slider" id="pitchSlider" min={0} max={2}
                                value={this.state.pitch} step={0.0001} onChange={this.changePitch.bind(this)}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("TTS_FRM_RATE")}</label>
                            <input type="range" className = "slider" id="rateSlider" min={0} max={1.5}
                                value={this.state.rate} step={0.0001} onChange={this.changeRate.bind(this)}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("TTS_FRM_SLEEP")}</label>
                            <input type="range" className = "slider" id="scanSlider" min={0} max={2000}
                                value={this.state.scansione} step={100} onChange={this.changeScansione.bind(this)}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("TTS_FRM_VOICE")}</label>
                            <Dropdown placeholder='Select Voice'
                                fluid search selection
                                options={this.state.voci}
                                defaultValue={this.state.selectedVoice}
                                onChange={this.changeVoice.bind(this)}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("TTS_FRM_HIGHLIGHTER")}</label>
                            <Radio
                                label={t("TTS_FRM_HIGHLIGHTER_CARD")}
                                name='radioGroup'
                                value='card'
                                checked={this.state.evidenziatore === 'card' ? true : false}
                                onChange={this.changeEvidenziatore.bind(this)}
                            />
                            </Form.Field>
                            <Form.Field>
                            <Radio
                                label={t("TTS_FRM_HIGHLIGHTER_INPUT")}
                                name='radioGroup'
                                value='text'
                                checked={this.state.evidenziatore === 'text' ? true : false}
                                onChange={this.changeEvidenziatore.bind(this)}
                            />
                        </Form.Field>
                            <Button onClick={this.playTest.bind(this)}>Test</Button>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button.Group>
                        <Button color='red' onClick={this.props.handleOptions.bind(this)}>{t("MAIN_BTN_CLOSE")}</Button>
                        <Button.Or/>
                        <Button color='green' onClick={this.saveOptions.bind(this)}>{t("HEAD_BTN_SAVE")}</Button>
                    </Button.Group>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default translate('translations')(SpeakOptions)
