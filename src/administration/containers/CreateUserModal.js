import React, { Component } from 'react'
import { Modal,Button, Popup, Icon } from 'semantic-ui-react'

import ErrorMessage from '../components/ErrorMessage'
import PositiveMessage from '../components/PositiveMessage'
import CreateUserForm from '../components/CreateUserForm'
import {apolloFetch} from '../../store/apolloFetchUtils'
import {escapeQuotes} from '../../store/functionUtils'

import {connect} from 'react-redux'

class CreateUserModal extends Component {
  constructor(props){
    super(props)
    this.state={
      team: this.props.toTeam,
      newName: '',
      newEmail: '',
      newUsername: '',
      newPassword: '',
      newLinkWeb: '',
      newOrg: '',
      newRole: '',
      newPermissions: '',
      userCreated: false,
      addedToTeam: false,
      error: false,
      loading: false,
      userDuplicateKey: false
    }
  }
  newNameChange(e){
    this.setState({newName: e.target.value})
  }
  newEmailChange(e){
    this.setState({newEmail: e.target.value})
  }
  newOrgChange(e){
    this.setState({newOrg: e.target.value})
  }
  newLinkWebChange(e){
    this.setState({newLinkWeb: e.target.value})
  }
  newUsernameChange(e){
    this.setState({newUsername: e.target.value})
  }
  newPassChange(e){
    this.setState({newPassword: e.target.value})
  }
  selectRole(e,data){
    let permissions = this.props.roles.filter((role)=>{
      if(role.id === data.value) return role
    })
    this.setState({newRole: data.value, newPermissions: escapeQuotes(permissions[0].role_permissions)})
  }
  createUser(){
    let query = `
      mutation newUser {
          createCaaUser(
              name: "${this.state.newName}",
              email: "${this.state.newEmail}",
              password: "${this.state.newPassword}",
              user: "${this.state.newUsername}",
              organization: "${this.state.newOrg}",
              link_web: "${this.state.newLinkWeb}",
              role_id: ${this.state.newRole},
              user_permissions: "${this.state.newPermissions}",
              idstaff: ${this.props.idstaff}
            ){
          id
        }
      }`
    //create user mutation
    this.setState({ loading: true })
    apolloFetch({query})
      .then((data)=>{
        if(!data.hasOwnProperty("errors")){
          //no errors returned, user created correctly
          let query= `
              mutation addUserTeam{
                createCaaUserTeam(iduser: ${data.data.createCaaUser.id}, idteam: ${this.state.team}){
                  id
                }
              }
            `
          //after being correctly created, adds user to correct team
          apolloFetch({query})
            .then((data)=>{
              if(!data.hasOwnProperty("errors")){
                //user added to team
                this.setState({error: false, userCreated: true, addedToTeam: true})
              }else{
                //errors returned, user created but not added to any team
                this.setState({error: true, userCreated:true, addedToTeam:false})
              }
              this.setState({ loading:false })
            })
            .catch((error)=>{
              //errors returned, user created but not added to any team
              this.setState({error: true, userCreated:true, addedToTeam:false})
            })
        }else{
          //errors returned, user not created and therefore not added to team
          let indexErrorArray
          indexErrorArray = data.errors.filter(error => error.message.search('1062'))
          this.setState({error: true, userCreated:false, addedToTeam:false, userDuplicateKey: indexErrorArray.length > 0 ? true : false})
        }

      })
      .catch((error)=>{
        this.setState({error: true, userCreated:false, addedToTeam:false})
      })
  }

  componentWillMount(){
    this.props.teams.map((team)=>{
      if(team.id === this.props.toTeam){
        this.setState({teamName: team.name})
      }
    })
  }
  getModalContent(){
    if(!this.state.error && !this.state.userCreated && !this.state.addedToTeam){
      //user yet to create
      return(
        <div>
          <CreateUserForm
            name = {this.state.newName}
            newNameChange = {()=>this.newNameChange.bind(this)}
            email = {this.state.newEmail}
            newEmailChange = {()=>this.newEmailChange.bind(this)}
            username = {this.state.newUsername}
            newUsernameChange = {()=>this.newUsernameChange.bind(this)}
            password = {this.state.newPassword}
            newPassChange = {()=>this.newPassChange.bind(this)}
            linkWeb = {this.state.newLinkWeb}
            newLinkWebChange = {()=>this.newLinkWebChange.bind(this)}
            org = {this.state.newOrg}
            newOrgChange = {()=>this.newOrgChange.bind(this)}
            roles = {this.props.roles}
            selectRole = {()=>this.selectRole.bind(this)}
            loggedUserRoleID = {this.props.loggedUserRoleID}
            />
          <p>Per continuare, compila tutti i campi segnati con *</p>
        </div>
      )
    }else if(this.state.error && !this.state.userCreated && !this.state.addedToTeam){
      //error, user not created nor added to team
      if (this.state.userDuplicateKey) {        
        return(<ErrorMessage header="Errore" message="Utente non creato perchè esiste già un utente con la stessa mail o username inserito" hidden={false}/>)
      } else {
        return(<ErrorMessage header="Errore" message="Utente non creato per un errore, riprova più tardi o contatta il supporto" hidden={false}/>)
      }
    }else if(this.state.error && this.state.userCreated && !this.state.addedToTeam){
      //error, user created but non added to team
      return(<ErrorMessage header="Errore" message="Utente creato ma non aggiunto al team per un errore interno, puoi farlo manualmente con la scheda Aggiungi Utente" hidden={false}/>)
    }else if(!this.state.error && this.state.userCreated && this.state.addedToTeam){
      //positive, user created and added to team
      return(<PositiveMessage header="Perfetto!" message="Utente creato e aggiunto al team, tutto ok :)" hidden={false}/>)
    }else return(<div></div>)

  }
  getButtonsLayout(){
    //used to render the correct button configuration
    if(!this.state.error && !this.state.userCreated && !this.state.addedToTeam){
      //user yet to create
      return(
        <div>
          <Button negative onClick={()=>this.props.closeModal()}>Annulla</Button>
          <Button positive onClick={()=>this.createUser()}
                  disabled={
                    !this.state.newName || !this.state.newEmail
                    || !this.state.newUsername || !this.state.newPassword
                    || !this.state.newRole}
                  loading={this.state.loading}>Crea Utente</Button>
        </div>
      )
    }else if(!this.state.error && this.state.userCreated && this.state.addedToTeam){
      //user created
      return(
        <Button positive onClick={()=>this.props.closeModal()}>Finito!</Button>
      )
    }else{
      //error
      return(
        <Button negative onClick={()=>this.props.closeModal()}>Continua</Button>
      )
    }
  }
  render(){
    let contentLayout = this.getModalContent();
    let buttonsLayout = this.getButtonsLayout();
    return(
      <Modal closeIcon="close" open={this.props.openModal} onClose={this.props.closeModal}>
        <Modal.Header>
          Creazione di nuovo utente per il team {this.state.teamName}
          <Popup trigger={<Icon name='help' style={{float: 'right'}}/>}
            content={"Creazione di un nuovo utente, una volta creato verrà immediatamente aggiunto al team "+this.state.teamName}/>
        </Modal.Header>
        <Modal.Content>
          {contentLayout}
        </Modal.Content>
        <Modal.Actions>
          {buttonsLayout}
        </Modal.Actions>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    idstaff: state.user.idstaff,
    loggedUserRoleID: state.user.role_id
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CreateUserModal)
