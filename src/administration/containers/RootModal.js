import React, { Component } from 'react'

import { translate } from 'react-i18next'
import { connect } from 'react-redux'
import {closeModal} from '../../store/actions/AdministrationActions'

import CreateUserModal from './CreateUserModal'
import AddUserModal from './AddUserModal'
import ManageTeamModal from './ManageTeamModal'
import EditUserParamsModal from './EditUserParamsModal'
import CreateTeamModal from './CreateTeamModal'
class RootModal extends Component{
  render(){
    switch(this.props.content){
      case "create_usr":
        return(<CreateUserModal {...this.props}/>)
      case "add_usr":
        return(<AddUserModal {...this.props}/>)
      case "manage_team":
        return(<ManageTeamModal {...this.props}/>)
      case "edit_user_params":
        return(<EditUserParamsModal {...this.props}/>)
      case "create_team":
        return(<CreateTeamModal {...this.props}/>)
      default:
        return(<div></div>)
    }
  }
}
const mapStateToProps = (state) => {
  return {
    openModal: state.modalReducer.openModal,
    content: state.modalReducer.modalContent,
    toTeam: state.modalReducer.toTeam,
    roles: state.adminReducer.roles,
    teams: state.adminReducer.teams,
    users: state.adminReducer.users
  }
}
//aggiungere azioni per le modali
const mapDispatchToProps = (dispatch) => {
  return {
    closeModal: () => dispatch(closeModal())
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(translate('translations') (RootModal))
