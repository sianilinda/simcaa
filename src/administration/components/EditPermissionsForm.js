import React, { Component } from 'react'
import { translate } from 'react-i18next'

import {Form, Checkbox} from 'semantic-ui-react'
import { isValidObject } from '../../store/functionUtils'

class EditPermissionsForm extends Component{
  render(){
    const { t } = this.props
    let permissions = []
    if(isValidObject(this.props.permissions)){
      permissions = this.props.permissions
    }else{
      permissions = JSON.parse(this.props.permissions)
    }
    let permLayout = Object.keys(permissions).map((key,index)=>{
      if(index%4 === 0){
        let gLayout = []
        Object.keys(permissions).map((k,i)=>{
          if(i >= index && i < index+4){

            gLayout.push(
              <Form.Field key={i}>
                <label>{t(k)}</label>
                <Checkbox toggle
                  disabled={!permissions[k] && !this.props.originalPermission[k]}
                  checked={permissions[k]}
                  onChange={()=>this.props.handlePermissionChange(k)}
                  />
              </Form.Field>
            )
          }
        })
        return(
          <Form.Group widths='equal' style={{marginTop: '15px'}} key={index}>
            {gLayout}
          </Form.Group>
        )
      }
    })
    return(
        <Form>
          {permLayout}
        </Form>
    )
  }
}
export default translate('translations')(EditPermissionsForm)
