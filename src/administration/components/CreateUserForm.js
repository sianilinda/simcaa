import React, { Component } from 'react'
import { Form, Select } from 'semantic-ui-react'

import { translate } from 'react-i18next'


class  CreateUserForm extends Component {
  componentWillMount(){
    let loggedUserRoleIndex = this.props.roles.findIndex(role => role.id === this.props.loggedUserRoleID)
    let tmpRoles = this.props.roles.filter(role => this.props.roles[loggedUserRoleIndex].weight >= role.weight)
    tmpRoles = tmpRoles.map((role)=>{
      return({
        text: role.role_desc,
        value: role.id
      })
    });
    this.setState({
      roles: tmpRoles
    });
  }
  render(){
    const { t } = this.props
    return(
      <Form>
        <Form.Field required error={this.props.nameError}>
          <label>{t("TBL_NAME")}</label>
          <input placeholder={t("TBL_NAME")}
                 value={this.props.name}
                 onChange={this.props.newNameChange()}
                 />
        </Form.Field>
        <Form.Field required>
          <label>{t("TBL_EMAIL")}</label>
          <input placeholder={t("TBL_EMAIL")}
                 value={this.props.email}
                 onChange={this.props.newEmailChange()}
                 />
        </Form.Field>
        <Form.Field required>
          <label>{t("TBL_USR")}</label>

          <input placeholder={t("TBL_USR")}
            value={this.props.username}
            onChange={this.props.newUsernameChange()}
            />
        </Form.Field>
        <Form.Field required>
          <label>Password</label>
          <input placeholder='Password'
                 type='password'
                 value={this.props.password}
                 onChange={this.props.newPassChange()}
                 />
        </Form.Field>
        <Form.Group widths='equal'>
          <Form.Field>
            <label>{t("TBL_LWEB")}</label>
            <input placeholder={t("TBL_LWEB")}
                   value={this.props.linkWeb}
                   onChange={this.props.newLinkWebChange()}
                   />
          </Form.Field>
          <Form.Field>
            <label>{t("TBL_ORG")}</label>
            <input placeholder={t("TBL_ORG")}
                   value={this.props.org}
                   onChange={this.props.newOrgChange()}
                   />
          </Form.Field>
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Field required>
            <label>{t("TBL_GROUP")}</label>
            <Select options={this.state.roles}
                    onChange={this.props.selectRole()}
                    />
          </Form.Field>
        </Form.Group>
      </Form>
    )
  }
}
export default (translate('translations')(CreateUserForm))
