import React,{Component} from 'react'
import { Message } from 'semantic-ui-react'

class PositiveMessage extends Component{
  render(){
    return(
      <Message positive hidden={this.props.hidden} onDismiss={this.props.onDismiss}>
        <Message.Header>{this.props.header}</Message.Header>
        <p>{this.props.message}</p>
      </Message>
    )
  }
}

export default PositiveMessage
