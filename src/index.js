import React from 'react'
import ReactDOM from 'react-dom'
import { MemoryRouter } from 'react-router-dom'

import { Provider } from 'react-redux'
import store from './store/configureStore'

import App from './App'

// i18n import
import { I18nextProvider } from 'react-i18next'
import i18n from './i18n'

// disable REACT DEVTOOLS
// if (window.__REACT_DEVTOOLS_GLOBAL_HOOK__) {
//     window.__REACT_DEVTOOLS_GLOBAL_HOOK__._renderers = {}
// }

const initialEntries = sessionStorage.getItem('history') ? sessionStorage.getItem('history').split() : '/'.split()

ReactDOM.render(
    <Provider store={store}>
        <I18nextProvider i18n={ i18n }>
            <MemoryRouter initialEntries={initialEntries} initialIndex={0}>
                <App />
            </MemoryRouter>
        </I18nextProvider>
    </Provider>,
    document.getElementById('root')
)
